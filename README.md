[![Run Status](https://api.shippable.com/projects/57a8cd49a353270c008c5380/badge?branch=master)](https://app.shippable.com/projects/57a8cd49a353270c008c5380) [![Coverage Badge](https://api.shippable.com/projects/57a8cd49a353270c008c5380/coverageBadge?branch=master)](https://app.shippable.com/projects/57a8cd49a353270c008c5380)

# CakePHP Cache Shell Shim

CakePHP 3.3 has a useful [Cache Shell](http://api.cakephp.org/3.3/class-Cake.Shell.CacheShell.html) which can be used to clear caches from the command line. This is useful for deployment scripts.

This shim backports this shell so it can be used in projects tied to older versions of CakePHP 3 (for example, if your host uses PHP 5.4 and it is not possible to upgrade to CakePHP 3.3).

## Platform Support

* This plugin is for use with CakePHP versions >=3.0, <3.3
* Supported PHP versions: 5.4, 5.5 and 5.6

## Installation

Install with composer:

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/chris48cf/cakephp-cache-shell-shim"
    }
],
"require": {
    "chris48cf/cakephp-cache-shell-shim": "dev-master"
}
```

## Usage

* Add the code `Plugin::load('Chris48cf/CacheShellShim');` to your project's `bootstrap.php`.
* The following shell commands are now available:

Show a list of all defined cache prefixes:

```
bin/cake cache list_prefixes
```

Clear all caches:

```
bin/cake cache clear_all
```

Clear the cache for a particular prefix:

```
bin/cake cache clear <prefix>
```
